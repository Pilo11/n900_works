package mainpackage;

import javafx.application.Application;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import manager.EncryptionManager;
import manager.FacadeHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EncrypterMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        List<File> files = FacadeHelper.getInstance().getSelectedFiles(primaryStage);
        List<Exception> exceptions = new ArrayList<>();
        if (files != null) {
            files.parallelStream().forEach(item -> {
                try {
                    EncryptionManager.getInstance().encrypt(item);
                } catch (Exception e) {
                    exceptions.add(e);
                }
            });
        }
        FacadeHelper.getInstance().showErrors(exceptions);
        if (exceptions.isEmpty())
            System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
