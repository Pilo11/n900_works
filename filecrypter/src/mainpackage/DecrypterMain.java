package mainpackage;

import javafx.application.Application;
import javafx.stage.Stage;
import manager.EncryptionManager;
import manager.FacadeHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by nutzer on 1/12/17.
 */
public class DecrypterMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        List<File> files = FacadeHelper.getInstance().getSelectedFiles(primaryStage);
        List<Exception> exceptions = new ArrayList<>();
        if(files != null)
        {
            files.parallelStream().forEach(item -> {
                try {
                    EncryptionManager.getInstance().decrypt(item);
                } catch (Exception e) {
                    exceptions.add(e);
                }
            });
        }
        FacadeHelper.getInstance().showErrors(exceptions);
        if (exceptions.isEmpty())
            System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
