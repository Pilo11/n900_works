package manager;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;

/**
 * Created by nutzer on 1/12/17.
 */
public class FacadeHelper {
    private static FacadeHelper ourInstance = new FacadeHelper();

    public static FacadeHelper getInstance() {
        return ourInstance;
    }

    private FacadeHelper() {
    }

    public List<File> getSelectedFiles(Stage stage) {
        FileChooser chooser = new FileChooser();
        return chooser.showOpenMultipleDialog(stage);
    }

    /**
     * shows an information box.
     * @param message
     */
    public void showInformation(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(message);
        alert.show();
    }

    public void showErrors(List<Exception> exceptions) {
        for (Exception ex :
                exceptions) {
            showError(ex);
        }
    }

    public void showError(Exception ex)  {
        showError(ex.getMessage());
    }

    private void showError(String exceptionMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setContentText(exceptionMessage);
        alert.show();
    }
}
