package manager;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.util.Base64;
import java.util.Random;

/**
 * Created by nutzer on 1/10/17.
 */
public class EncryptionManager {
    private static EncryptionManager ourInstance = new EncryptionManager();

    public static EncryptionManager getInstance() {
        return ourInstance;
    }

    private EncryptionManager() {

    }

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";
    private static final int PASSWORD_RESOLUTION = 256;
    private static final String KEYFILE_SUFFIX = ".key";

    /**
     * encrypt a file with a random AES key, the key will be saved in a key file.
     * @param inputFile
     * @throws Exception
     */
    public void encrypt(File inputFile)
            throws Exception {
        byte[] keyBytes = getRandomKey(PASSWORD_RESOLUTION);
        String keyFilePath = Paths.get(inputFile.getParent(), encodeFileName(inputFile.getName()) + KEYFILE_SUFFIX).toString();
        File keyFile = new File(keyFilePath);
        saveFile(keyFile, keyBytes);

        String outputFilePath = Paths.get(inputFile.getParent(), encodeFileName(inputFile.getName())).toString();
        File outputFile = new File(outputFilePath);
        doCrypto(Cipher.ENCRYPT_MODE, keyBytes, inputFile, outputFile);
    }

    public void decrypt(File inputFile)
            throws Exception {
        String outputFilePath = Paths.get(inputFile.getParent(), decodeFileName(inputFile.getName())).toString();
        File outputFile = new File(outputFilePath);
        String keyFilePath = inputFile.getAbsolutePath() + KEYFILE_SUFFIX;
        File keyFile = new File(keyFilePath);
        if (!keyFile.exists())
            throw new Exception("Could not find the key file to decrypt this file.");
        byte[] keyBytes = getFileContent(keyFile);
        doCrypto(Cipher.DECRYPT_MODE, keyBytes, inputFile, outputFile);
    }

    private void doCrypto(int cipherMode, byte[] keyBytes, File inputFile,
                                 File outputFile) throws Exception {
        try {
            Key secretKey = new SecretKeySpec(keyBytes, ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            CipherInputStream cis = new CipherInputStream(inputStream, cipher);

            Long filesize = inputFile.length();
            Long counter = new Long(0);
            String fileName = (cipherMode == Cipher.ENCRYPT_MODE) ? inputFile.getName() : outputFile.getName();
            long roundedPercentage = 0;
            int read;
            while((read = cis.read())!=-1)
            {
                outputStream.write((char)read);
                outputStream.flush();
                counter++;
                Double percentage = (Double.valueOf(counter) / Double.valueOf(filesize)) * 100;
                roundedPercentage = Math.round(percentage);
                if(counter % 10000000 == 0){
                    System.out.println(fileName + " progess: " + String.valueOf(roundedPercentage) + "%");
                }
            }
            System.out.println(fileName + " finished!");

            inputStream.close();
            outputStream.close();
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | IOException ex) {
            throw new Exception("Error encrypting/decrypting file", ex);
        }
    }

    /**
     * gets the content of a file as byte array.
     * @param file
     * @return
     * @throws IOException
     */
    private byte[] getFileContent(File file) throws IOException {
        return Files.readAllBytes(Paths.get(file.getAbsolutePath()));
    }

    /**
     * saves a byte array as file content in a file.
     * @param outputFile
     * @param fileContent
     * @throws IOException
     */
    private void saveFile(File outputFile, byte[] fileContent) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(outputFile);
        outputStream.write(fileContent);
        outputStream.close();
    }

    private String encodeFileName(String fileName) {
        Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(fileName.getBytes());
    }

    private String decodeFileName(String encodedFileName) {
        return new String(Base64.getUrlDecoder().decode(encodedFileName));
    }

    /**
     * generates a random key string and returns the byte array of its characters.
     * @param passwordResolution
     * @return
     */
    private byte[] getRandomKey(final int passwordResolution) throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(passwordResolution); // for example
        SecretKey secretKey = keyGen.generateKey();
        return secretKey.getEncoded();
    }
}
