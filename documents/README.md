# Inhalt

- Nokia N900 neu flashen [hier](flash/tut_flash.md)
- Java 8 Installation auf dem Nokia N900 [hier](java8/tut_java8.md)
- Java 8 Swing Applikationen auf dem Nokia N900 [hier](java8-headful/tut_java8-headful.md)
- Arch Linux ARM auf externe Micro-SD Karte installieren [hier](archinstall/tut_archinstall.md) (noch nicht fertig)
