# RootSh Tutorial

## Präambel

Dieses Dokument gereicht hoffentlich allen "Bastler-n/innen/ix", die sich mit dem Nokia N900 beschäftigen, zur Hilfe, den Programmmanager korrekt zu konfigurieren. Diese Anleitung ist eigentlich nur für absolute Neulinge gedacht, da diese Schritte von jedem/r erfahrenen Bastler-n/in/ix schnell selbstgemacht sind, ohne eine Anleitung zu benötigen.

## Voraussetzungen

Was wird alles dafür benötigt?

- ein technisch einwandfreies Nokia N900
- ein funktionierendes, internetangebundenes WLAN-Netzwerk

Was wird im Tutorial benutzt?

- ein technisch einwandfreies Nokia N900
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk

## Benötigte Dateien

**Keine**

### Besonderes

**Nichts**

## Kochrezept

1. Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900 und gehe ins Hauptmenü (der Button mit den sechs blauen Quadraten oben links) und öffne den Progr.-Manager.

  ![n900_menu](images/n900_menu.png)

  Damit ist das Installieren von rootsh zur Freischaltung der root-Rechte auf dem Nokia N900 abgeschlossen.
1. Danach in das Hauptmenü wechseln und den Progr. Manager auswählen. Dort müssen veraltete Paketkataloge deaktiviert werden.

  ![n900_desktop](images/n900_menu.png)

  Im Programm-Manager oben auf Programmmanager drücken. Danach wird (falls noch nicht geschehen) nach einem WLAN-Netzwerk gefragt, mit dem sich das Gerät verbinden soll. Dieser Schritt ist erforderlich, da die Paketquellen noch aktualisiert werden. Nachdem eine aktive WLAN-Verbindung steht, kann es weitergehen.

  ![n900_katalog](images/n900_katalog.png)

  Jetzt alle Programmkataloge nacheinander auswählen, die rot markiert oder durch ein rotes Ausrufungszeichen oder ähnliches gekennzeichnet sind.

  ![n900_katalog_deactivated](images/n900_katalog_deactivated.png)

  In meinem Fall müssen die beiden Nokia Quellen und die `Ovi` Quelle deaktiviert werden, sodass sie ausgegraut sind (durch Draufdrücken und das Auswählen von `Deaktiviert`).
1. Als letztes zurücknavigieren und im Hauptmenü des Programm-Managers `Aktualisieren` auswählen.

Damit ist das Konfigurieren des Programm-Managers auf dem Nokia N900 abgeschlossen.
