#!/bin/bash4

# ------------------------------------------------------------------
# -------------------------- script header -------------------------
# ------------------------------------------------------------------

# colors
RED='\033[0;31m' # red
BLUE='\033[0;34m' # BLUE
GREEN='\033[0;32m' # green
NC='\033[0m' # no color
# text styles
BOLD='\033[1m' #$(tput bold)
NORMAL='\033[0m' #$(tput sgr0)

echo -e "This script was written/updated by Pilo11 (17-01-2017)"
echo -e "${RED}-----------------------------------------------${NC}"
echo -e "${RED}------------------ versions -------------------${NC}"
echo -e "${RED}-----------------------------------------------${NC}"
echo -e "17-01-2017 initial release ${BLUE}(Pilo11)${NC} | ${BOLD}v0.1${NORMAL}"
echo -e "${RED}-----------------------------------------------${NC}"
echo -e ""

# ------------------------------------------------------------------
# --------------------------- preamble -----------------------------
# ------------------------------------------------------------------
echo -e "This script will configure Java 8 HEADFUL for the Nokia N900 on Maemo 5"
echo -e "This script was tested. Despite this you run it at your own risk!"

echo -ne "${RED}${BOLD}Do you want to continue?${BOLD}${NC} (default: yes): "
read CONTIN
echo -e ""
if [ "${CONTIN,,}" != "yes" ] && [ "${CONTIN,,}" != "" ]
then
  echo -e "${RED}${BOLD}Script cancelled!${NC}${NORMAL}"
  exit 1 # 1: user cancelled
fi

# ------------------------------------------------------------------
# ------------------------ checking files --------------------------
# ------------------------------------------------------------------
PATH='files/'
echo -e "${BLUE}${BOLD}Checking for needed files... The files have to be in 'files/...'${NORMAL}${NC}"
# needed files
EJDK='ejdk-8u111-linux-armv6-vfp-hflt.tar.gz'
if [ ! -f "${PATH}${EJDK}" ]; then
  echo -e "${RED}Could not find the file '${EJDK}'... Aborting...${NC}"
  exit 2 # 2: file not found
else
  echo -e "${GREEN}${EJDK} found :-)${NC}"
fi
#OPENJFX='openjfx-armv6-sflt.tar.gz'
#if [ ! -f "${PATH}${OPENJFX}" ]; then
#  echo -e "${RED}Could not find the file '${OPENJFX}'... Aborting...${NC}"
#  exit 2 # 2: file not found
#else
#  echo -e "${GREEN}${OPENJFX} found :-)${NC}"
#fi

# ------------------------------------------------------------------
# -------------------------- script work ---------------------------
# ------------------------------------------------------------------

# set the folder where the java environment will be built to
JRE_BUILDED="/opt/jre_builded"

# Checks if java has already been installed.
if [ ! -L "/usr/bin/java" ] && [ ! -d "${JRE_BUILDED}" ]; then
  echo -e "${RED}Java is not installed... Aborting...${NC}"
  exit 3 # 3: script running not possible.
else
  echo -e "${GREEN}Java has already been installed... Continue...${NC}"
fi

echo -e "$(/bin/date +%T): copying the java environment tarball to /opt..."
/bin/cp "files/${EJDK}" /opt

echo -e "$(/bin/date +%T): changing working directory to /opt"
cd /opt

echo -e "$(/bin/date +%T): extracting the java headful Swing file from tarball..."
/bin/tar xzf "${EJDK}" ejdk1.8.0_111/linux_armv6_vfp_hflt/jre/lib/arm/libawt_xawt.so

echo -e "$(/bin/date +%T): moving file into Java 8 headless environment which converts to headful (Swing)..."
/bin/mv ejdk1.8.0_111/linux_armv6_vfp_hflt/jre/lib/arm/libawt_xawt.so "${JRE_BUILDED}/lib/arm/libawt_xawt.so"

echo -e "$(/bin/date +%T): deleting the tarball file..."
/bin/rm -rf "${EJDK}"

echo -e "$(/bin/date +%T): deleting the extracted directory..."
/bin/rm -rf "ejdk1.8.0_111"

echo -e "${GREEN}${BOLD}$(/bin/date +%T): finished configuring java headful :-D${NC}${NORMAL}"

echo -e "${BLUE}${BOLD}TODO: JavaFX not working${NC}${NORMAL}"

echo -e ""
echo -e "${GREEN}-----------------------------------------------${NC}"
echo -e "${GREEN}${BOLD}script successfully finished!${NC}${NORMAL}"
echo -e "${GREEN}-----------------------------------------------${NC}"
