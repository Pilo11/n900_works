# Java 8 (grafische Oberflächen) Tutorial

## Präambel

In diesem Dokument wird beschrieben, welche Schritte getan werden müssen, um auf dem Nokia N900 mit Maemo 5 eine Java Anwendung mit grafischer Oberfläche ausführen zu können (Swing). Beim Versuch, eine Java Swing Anwendung auszuführen, erscheint dann wahrscheinlich folgendes.

![swing_failed](images/swing_failed.png)

## Voraussetzungen

Was wird alles dafür benötigt?

- die erfolgreiche Durchführung dieser Anleitung (Java 8 (headless): [hier](../java8/tut_java8.md))

Was wird im Tutorial benutzt?

- ein Nokia N900 mit Maemo 5

## Benötigte Dateien

Wie gewohnt, befinden sich für dieses Tutorial alle benötigten Dateien verschlüsselt in der MEGA Cloud.

**Wie kann ich die verschlüsselten Dateien verwenden/entschlüsseln?**

Dazu gibt es [hier](../fileHandling/tut_fileHandling.md) eine Anleitung.

- Dateien für dieses Tutorial: [hier](https://mega.nz/#F!b0twWAKJ!xbWSNFn-7ZT0aj5Zaa_hpw)

- `ejdk-8u111-linux-armv6-vfp-hflt.tar.gz`: Java 8 Hard-FloatingPoint Embedded ARM version (headful)

### Besonderes

- `openjfx-armv6-sflt.tar.gz`: OpenJFX SoftFP ARM version (funktioniert noch nicht, bisher nicht nötig)

## Automatisches Skript

Für dieses Kochrezept ist ein automatisches Skript vorhanden, welches benutzt werden kann. Diese Skripte sind immer ".sh" Dateien und können einfach mit `bash4 filename.sh` auf dem Gerät (im `X-Terminal`) ausgeführt werden.

**Das setzt voraus, dass `Bash4` bereits installiert wurde (wie es [hier](../bash4/tut_bash4.md) beschrieben ist).**

1. Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900, schließe es an einen PC und ziehe die Datei `tut_java8_headful.sh` in das Hauptverzeichnis des Geräts.
1. Als nächstes müssen auch die Dateien (entschlüsselt) in einem Unterverzeichnis `files` auf das Gerät kopiert werden.
1. Danach wird X-Terminal geöffnet und folgendes eingegeben.

  `cd MyDocs`

  Zum Wechseln in das Hauptverzeichnis des internen Speichers.

  `bash4 tut_java8_headful.sh`

  Zum Ausführen des Skripts.

  Wenn dabei keine Fehler angezeigt werden, dann hat alles funktioniert.

## Kochrezept

Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900, schließe es an einen PC und ziehe die Datei `ejdk-8u111-linux-armv6-vfp-hflt.tar.gz` in das Hauptverzeichnis des Geräts.
1. Danach wird X-Terminal geöffnet und folgendes eingegeben.

    `cd MyDocs`

    Zum Wechseln in das Hauptverzeichnis des internen Speichers.

    `mv ejdk-8u111-linux-armv6-vfp-hflt.tar.gz /opt`

    Zum Verschieben des Archivs in die "Installations/System" Partition.

    `cd /opt`

    Zum Wechseln in das Arbeitsverzeichnis.

    `tar xzf ejdk-8u111-linux-armv6-vfp-hflt.tar.gz ejdk1.8.0_111/linux_armv6_vfp_hflt/jre/lib/arm/libawt_xawt.so`

    Zum Entpacken der einen Datei, die eine Java Umgebung `headful` macht und Java Swing Anwendungen ausführbar macht.

1. Die entpackte `libawt_xawt.so` Datei befindet sich jetzt in der Unterordnerstruktur in `ejdk1.8.0_111`. Sie muss daraus verschoben werden in `/opt/jre_builded/lib/arm`.

    `mv ejdk1.8.0_111/linux_armv6_vfp_hflt/jre/lib/arm/libawt_xawt.so /opt/jre_builded/lib/arm/libawt_xawt.so`

1. Danach können das Archiv und der erstellte Ordner `ejdk1.8.0_111` gelöscht werden.

1. Zum Testen der Swing Oberfläche, steht im Ordner `files` die Datei `helloWorldSwing.jar` unverschlüsselt zur Verfügung. Diese muss einfach nur auf das Gerät kopiert und dort ausgeführt werden.

    `java -jar helloWorldSwing.jar`

    ![swing_works](images/swing_works.png)

Jetzt können auch Java Anwendungen mit grafischer Oberfläche (Swing) auf dem Nokia N900 ausgeführt werden.
