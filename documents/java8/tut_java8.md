# RootSh Tutorial

## Präambel

Dieses Dokument gereicht hoffentlich allen "Bastler-n/innen/ix", die sich mit dem Nokia N900 beschäftigen, zur Hilfe, eine eingebette Version von Java 8 auf dem Nokia N900 unter `Maemo 5` zum Laufen zu bekommen. Dabei können nur `HEADLESS` Java Anwendungen ausgeführt werden, da die `ARM embedded Java 8 JRE` Version hierfür verwendet wird und diese kein JavaFx oder Swing unterstützt/enthält.

## Voraussetzungen

Was wird alles dafür benötigt?

- ein PC mit Linux-OS, Windows oder (Mac OSX nicht getestet)
- ein technisch einwandfreies Nokia N900
- ein funktionierendes, internetangebundenes WLAN-Netzwerk
- root-Rechte (Tutorial: [hier](../rootsh/tut_rootsh.md))

Was wird im Tutorial benutzt?

- ein Laptop mit Arch Linux (Dell Latitude E5540)
- ein technisch einwandfreies Nokia N900
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk

## Benötigte Dateien

Wie gewohnt, befinden sich für dieses Tutorial alle benötigten Dateien verschlüsselt in der MEGA Cloud.

**Wie kann ich die verschlüsselten Dateien verwenden/entschlüsseln?**

Dazu gibt es [hier](../fileHandling/tut_fileHandling.md) eine Anleitung.

- Dateien für dieses Tutorial: [hier](https://mega.nz/#F!e8FQgIbD!1rcS2c6t_IhOZo_JsH4Dbw)

- `ejdk-8u111-linux-arm-sflt.tar.gz`: Java 8 Embedded armv6 SoftFP.

### Besonderes

**Nichts**

## Automatisches Skript

Für dieses Kochrezept ist ein automatisches Skript vorhanden, welches benutzt werden kann. Diese Skripte sind immer ".sh" Dateien und können einfach mit `bash4 filename.sh` auf dem Gerät (im X-Terminal) ausgeführt werden.

**Das setzt voraus, dass `Bash4` bereits installiert wurde (wie es [hier](../bash4/tut_bash4.md) beschrieben ist).**

1. Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900, schließe es an einen PC und ziehe die Datei `tut_java8.sh` in das Hauptverzeichnis des Geräts.
1. Als nächstes müssen auch die Dateien (entschlüsselt) in einem Unterverzeichnis `files` auf das Gerät kopiert werden.
1. Danach wird X-Terminal geöffnet und folgendes eingegeben.

  `cd MyDocs`

  Zum Wechseln in das Hauptverzeichnis des internen Speichers.

  `bash4 tut_java8.sh`

  Zum Ausführen des Skripts.

  Wenn dabei keine Fehler angezeigt werden, dann hat alles funktioniert.

## Manuelles Kochrezept

1. Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900, schließe es an einen PC und ziehe die Datei `ejdk-8u111-linux-arm-sflt.tar.gz` in das Hauptverzeichnis des Geräts.
1. Danach wird X-Terminal geöffnet und folgendes eingegeben.

    `cd MyDocs`

    Zum Wechseln in das Hauptverzeichnis des internen Speichers.

    `mv ejdk-8u111-linux-arm-sflt.tar.gz /opt`

    Zum Verschieben des Archivs in die "Installations/System" Partition.

    `cd /opt`

    Zum Wechseln in das Arbeitsverzeichnis.

    `tar xzf ejdk-8u111-linux-arm-sflt.tar.gz`

    Zum Entpacken der vorkompilierten, eingebetteten Java Umgebung. Dieser Vorgang kann etwas länger dauern (~4-6min auf dem Nokia N900).

1. Als nächstes muss die `JAVA_HOME` Variable gesetzt werden. In meinem Fall sieht das ganze so aus.

    `export JAVA_HOME=/opt/ejdk1.8.0_111/linux_arm_sflt/jre`
1. Danach muss der Konfigurator/Builder der Java Umgebung gestartet werden. Dort kann gleichzeitig ausgewählt werden, in welchem Zielpfad die Java Umgebung zu erreichen ist. Ich wähle in diesem Beispiel `/opt/jre_builded` als Zielpfad.

    `cd /`

    Zum Wechseln ins root-Verzeichnis.

    `./opt/ejdk-8u111/bin/jrecreate.sh --dest /opt/jre_builded`

    Dieser Vorgang dauert eine sehr lange Zeit (~20-25h). Also hol dir schonmal einen Tee und eine Spielekonsole.
1. Als letztes muss die Java Umgebung nur noch ausführbar gemacht werden und in `/usr/bin` verlinkt werden.

    `chmod +x /opt/jre_builded/bin/java`

    Vergibt die Rechte zum Ausführen.

    `ln -s /opt/jre_builded/bin/java /usr/bin`

    Verlinkt die Java Umgebung in /usr/bin und macht Java so von überall ausführbar.
1. Im Ordner `files` befindet sich eine unverschlüsselte Datei namens `helloWorld.jar`. Diese kann auf das Gerät kopiert werden und dort (zum Testen) mit `java -jar helloWorld.jar` ausgeführt werden.

    ![helloWorld](images/helloWorld.png)

Damit ist das Installieren und Konfigurieren von Java 8 JRE auf dem Nokia N900 abgeschlossen.
