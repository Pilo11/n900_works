#!/bin/bash4

# ------------------------------------------------------------------
# -------------------------- script header -------------------------
# ------------------------------------------------------------------

# colors
RED='\033[0;31m' # red
BLUE='\033[0;34m' # BLUE
GREEN='\033[0;32m' # green
NC='\033[0m' # no color
# text styles
BOLD='\033[1m' #$(tput bold)
NORMAL='\033[0m' #$(tput sgr0)

echo -e "This script was written/updated by Pilo11 (13-01-2017)"
echo -e "${RED}-----------------------------------------------${NC}"
echo -e "${RED}------------------ versions -------------------${NC}"
echo -e "${RED}-----------------------------------------------${NC}"
echo -e "13-01-2017 initial release ${BLUE}(Pilo11)${NC} | ${BOLD}v0.1${NORMAL}"
echo -e "${RED}-----------------------------------------------${NC}"
echo -e ""

# ------------------------------------------------------------------
# --------------------------- preamble -----------------------------
# ------------------------------------------------------------------
echo -e "This script will configure Java 8 for the Nokia N900 on Maemo 5"
echo -e "This script was tested. Despite this you run it at your own risk!"

echo -ne "${RED}${BOLD}Do you want to continue?${BOLD}${NC} (default: yes): "
read CONTIN
echo -e ""
if [ "${CONTIN,,}" != "yes" ] && [ "${CONTIN,,}" != "" ]
then
  echo -e "${RED}${BOLD}Script cancelled!${NC}${NORMAL}"
  exit 1 # 1: user cancelled
fi

# ------------------------------------------------------------------
# ------------------------ checking files --------------------------
# ------------------------------------------------------------------
PATH='files/'
echo -e "${BLUE}${BOLD}Checking for needed files... The files have to be in 'files/...'${NORMAL}${NC}"
# needed files
EJDK='ejdk-8u111-linux-arm-sflt.tar.gz'
if [ ! -f "${PATH}${EJDK}" ]; then
  echo -e "${RED}Could not find the file '${EJDK}'... Aborting...${NC}"
  exit 2 # 2: file not found
else
  echo -e "${GREEN}${EJDK} found :-)${NC}"
fi

# ------------------------------------------------------------------
# -------------------------- script work ---------------------------
# ------------------------------------------------------------------

# set the folder where the java environment will be built to
JRE_BUILDED="/opt/jre_builded"

# Checks if java has already been installed.
if [ ! -L "/usr/bin/java" ] && [ ! -d "${JRE_BUILDED}" ]; then
  echo -e "${GREEN}Java is not installed, will continue...${NC}"
else
  echo -e "${RED}Java has already been installed... Aborting...${NC}"
  exit 3 # 3: script running not necessary.
fi

echo -e "$(/bin/date +%T): copying the java environment tarball to /opt..."
/bin/cp "files/${EJDK}" /opt

echo -e "$(/bin/date +%T): changing working directory to /opt"
cd /opt

echo -e "$(/bin/date +%T): extracting the java environment tarball..."
/bin/tar xzf "${EJDK}"

EJDKHOME="/opt/ejdk1.8.0_111"
echo -e "$(/bin/date +%T): setting the EJDK_HOME variable..."
export EJDK_HOME="${EJDKHOME}"

echo -e "$(/bin/date +%T): setting the JAVA_HOME variable..."
export JAVA_HOME="${EJDKHOME}/linux_arm_sflt/jre"

echo -e "$(/bin/date +%T): give execution rights to the java jre creator..."
/bin/chmod +x "${EJDKHOME}/bin/jrecreate.sh"

echo -e "$(/bin/date +%T): changing working directory to /"
cd /

echo -e "$(/bin/date +%T): create the JAVA environment in ${JRE_BUILDED}, please wait ~20-25h..."
/bin/sh ${EJDKHOME}/bin/jrecreate.sh --dest "${JRE_BUILDED}"

echo -e "$(/bin/date +%T): give execution rights to the java binary..."
/bin/chmod +x "${JRE_BUILDED}/bin/java"

echo -e "$(/bin/date +%T): create links for the java binary in /usr/bin..."
/bin/ln -s "${JRE_BUILDED}/bin/java" /usr/bin

echo -e "$(/bin/date +%T): deleting the tarball file..."
/bin/rm -rf "/opt/${EJDK}"

echo -e "$(/bin/date +%T): deleting the ejdk source directory..."
/bin/rm -rf "${EJDKHOME}"

echo -e "${GREEN}${BOLD}$(/bin/date +%T): finished installing java :-D${NC}${NORMAL}"

echo -e ""
echo -e "${GREEN}-----------------------------------------------${NC}"
echo -e "${GREEN}${BOLD}script successfully finished!${NC}${NORMAL}"
echo -e "${GREEN}-----------------------------------------------${NC}"
