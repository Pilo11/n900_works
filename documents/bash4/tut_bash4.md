# Bash4 Tutorial

## Präambel

Dieses Dokument demonstriert die Installation der `Bash4`-Shell, die für die automatischen Skripte der Tutorials erforderlich ist.

## Voraussetzungen

Was wird alles dafür benötigt?

- ein technisch einwandfreies Nokia N900
- ein funktionierendes, internetangebundenes WLAN-Netzwerk
- root-Rechte (Tutorial: [hier](../rootsh/tut_rootsh.md))

Was wird im Tutorial benutzt?

- ein technisch einwandfreies Nokia N900
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk

## Benötigte Dateien

**Keine**

### Besonderes

**Nichts**

## Kochrezept

1. Zuerst muss `X-Terminal` auf dem N900 geöffnet werden. Dann folgen folgende Befehle.

  Zum Anfordern der root-Rechte.

  `sudo gainroot`

  Zum Installieren von `Bash4`.

  `apt-get install bash4`

Damit ist `Bash4` installiert worden.
