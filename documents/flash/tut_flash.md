# Flash Tutorial

## Präambel

Dieses Dokument gereicht hoffentlich allen "Bastler-n/innen/ix", die sich mit dem Nokia N900 beschäftigen, zur Hilfe ein funktionierendes, sauberes, neues Maemo 5 OS auf dem Gerät zu installieren (original).

## Voraussetzungen

Was wird alles dafür benötigt?

- ein funktionierendes Linux-OS
- ein technisch einwandfreies Nokia N900
- ein funktionierendes Micro-USB Kabel
- ein funktionierendes, internetangebundenes WLAN-Netzwerk
- VirtualBox eingerichtet mit einem Windows XP SP3 System
- die bereitgestellten Dateien zum Thema Flashing

Was wird im Tutorial benutzt?

- ein Laptop mit Arch Linux (Dell Latitude E5540)
- ein technisch einwandfreies Nokia N900
- ein "com-pad" Micro-USB Billig-Kabel
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk
- VirtualBox für ArchLinux (bekanntes USB Input Problem für das Guest OS ist vorher behoben worden)
- die bereitgestellten Dateien

## Benötigte Dateien

Wie gewohnt, befinden sich für dieses Tutorial alle benötigten Dateien verschlüsselt in der MEGA Cloud.

**Wie kann ich die verschlüsselten Dateien verwenden/entschlüsseln?**

Dazu gibt es [hier](../fileHandling/tut_fileHandling.md) eine Anleitung.

- Dateien für dieses Tutorial: [hier](https://mega.nz/#F!nttVEBBA!FUxL2hbVXiiVP2M56eZJ-g)

- `maemo_flasher-3.5_2.5.2.2.exe`: Enthält einen Installationsassistenten, der die benötigten Treiber und eine kleine Anwendung zum Flashen des Nokia N900 bereitstellt.
- `RX-51_2009SE_10.2010.13-2.VANILLA_PR_EMMC_MR0_ARM.bin`:  Enthält Informationen zum Flashen des eMMC (embedded MMC) und erstellt ein fertiges Dateisystem (`/home` Partition). Innerhalb dieser Partition befinden sich dann Ordner/Strukturen für Dokumente, Bilder und weitere persönliche Einstellungen (VFAT).
- `RX-51_2009SE_20.2010.36-2.003_PR_COMBINED_003_ARM.bin`: Enthält Informationen zum Flashen des `rootfs`, `kernel` und `bootloader`.

### Besonderes

Im Ordner `files/custom` befindet sich ebenfalls eine eMMC Konfiguration. Diese ist allerdings angepasst worden und kann **stattdessen** verwendet werden.

**Was bietet diese angepasste Konfiguration für Vorteile?**

- 5GB Speicherplatz für installierte Anwendungen/Programme statt 2GB.

## Kochrezept

1. Man/Frau/Hermaphrodit/Neutrum starte das "Windows XP SP3" OS in VirtualBox.

  ![desktop](images/xp_vm_desktop.png)

1. Kopiere alle Dateien aus dem `files` Ordner auf das XP System.

  **WICHTIG**

  An dieser Stelle **muss** entschieden werden, ob die originale oder die angepasste Version der eMMC-Konfiguration verwendet werden soll! Eine von beiden Dateien muss dann hinüberkopiert werden.

  ![files](images/xp_vm_desktop_files.png)

1. Führe die `maemo_flasher-3.5...` Installation aus.
1. Danach lösche die Installtionsdatei und verschiebe die anderen beiden Binary-Dateien in folgenden Order `C:\Program Files\maemo\flasher-3.5`.
1. Jetzt führe den Maemo-Flasher aus oder öffne `cmd` und wechsel in dieses Verzeichnis.

  ![flasher_start](images/xp_vm_flasher_start.png)

1. Nun schalte dein Nokia N900 Gerät aus und stelle sicher, dass das USB-Kabel nicht am Gerät angeschlossen ist, schiebe das Tastaturenfeld auf und halte die Taste `u` gedrückt. Während dieser Zeit, schließe das USB-Kabel am Gerät an (und am PC logischerweise), ohne die `u` Taste loszulassen. Sobald das Gerät reagiert, halte die Taste noch etwa drei Sekunden gedrückt und lasse sie dann los.
1. Als nächstes muss das angeschlossene Gerät mit dem VirtualBox-OS verbunden werden.

  ![vm_usb](images/vm_usb.png)

1. Danach muss folgender Befehl eingegeben werden zum Flashen `flasher-3.5.exe -F RX-51_2009SE_20.2010.36-2.003_PR_COMBINED_003_ARM.bin -f`.
1. Wenn der Vorgang erfolgreich abgeschlossen ist, muss als nächstes dieser Befehl ausgeführt werden

  `flasher-3.5.exe -F RX-51_2009SE_10.2010.13-2.VANILLA_PR_EMMC_MR0_ARM.bin -f -R`

  Dabei kann es passieren, dass die Konsole anzeigt:

  `Suitable USB device not found, waiting`

  Sollte dies der Fall sein, so muss das Nokia Gerät erneut unter `Devices -> USB -> Nokia N900 ...` im VirtualBox Menü ausgewählt werden.
1. Das Gerät sollte sich jetzt von selbst neustarten, ansonsten muss dies jetzt manuell geschehen.
1. Nach dem Start des Geräts, bitte Datum und Uhrzeit eingeben und bestätigen. Danach müsste der Desktop sichtbar sein.

Damit ist das Flashen/Neuinstallieren des `Maemo 5` Systems auf dem Nokia N900 abgeschlossen.
