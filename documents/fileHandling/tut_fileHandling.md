# Dateiverwaltung Tutorial

## Präambel

Dieses Dokument gereicht hoffentlich allen "Bastler-n/innen/ix", die sich mit Dokumenten dieser Quelle beschäftigen, zur Hilfe, benötigte Dateien für die einzelnen Tutorials herunterzuladen und verwendbar zu machen.

## Voraussetzungen

Was wird alles dafür benötigt?

- das Decrypt-Tool (JavaFX)
- ein funktionierendes, internetangebundenes WLAN-Netzwerk

Was wird im Tutorial benutzt?

- ein PC mit Linux/Mac OSX/Windows und Java 8 SDK oder JRE (Oracle-Version)
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk

## Benötigte Dateien

- das Decrypter-Tool liegt im `files` Ordner und heißt `fileDecrypter.jar`

### Besonderes

- im Gegensatz zu den anderen Tutorials befindet sich die benötigte Datei unverschlüsselt, direkt im `files` Ordner
- der Source-Code für das Verschlüsselungs/Entschlüsselungs-Tool ist vollständig in diesem Repository enthalten und befindet sich im `filecrypter` Ordner (ich verwende IntelliJ)

## Kochrezept

1. In dem entsprechenden Tutorial befinden sich in den `files` Ordnern jeweils die `KEY`-Dateien, welche das Entschlüsselungspasswort für die einzelnen Dateien enthalten. Die benötigten, verschlüsselten Dateien sind im angegebenen Cloud-Ordner abgelegt (MEGA.co.nz Link). Die Dateien müssen aus dem Cloud-Ordner heruntergeladen und in den files Ordner platziert werden. Wichtig ist dabei, dass die `KEY`-Dateien in denselben Ordnern liegen wie die
1. Danach sollte das Decrypt-Tool geöffnet werden.

  Unter Linux zum Beispiel zum Öffnen:

  `java -jar fileDecrypter.jar`

1. Es erscheint ein Dateiauswahlfenster. Damit sollte man/frau/hermaphrodit/neutrum in das entsprechende `files` Verzeichnis des Tutorials wechseln. Dort müssen dann alle zu entschlüsselnden Dateien ausgewählt werden (Mehrauswahl möglich innerhalb eines Ordners).
1. Das Entschlüsseln dauert bei den Dateien einige Zeit (pro 100MB etwa 2min, je nach Ressourcen des PCs).

  Beispiel:

  ![fileDecrypter](images/filedecrypter.png)

  Wenn die Anwendung von der Konsole aus gestartet wurde, dann ist der aktuelle Status auf der Konsole sichtbar. Ansonsten muss einfach gewartet werden, bis das Tool fertig ist.

  ![fileDecrypting](images/file_decrypting.png)

  Nach dem Entschlüsselungsvorgang sieht das ganze dann so aus:

  ![decrypted_file](images/file_decrypted.png)

Jetzt sollte jeder in der Lage sein, die benötigten Dateien für die einzelnen Tutorials verwendbar zu machen.
