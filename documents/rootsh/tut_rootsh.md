# RootSh Tutorial

## Präambel

Dieses Dokument gereicht hoffentlich allen "Bastler-n/innen/ix", die sich mit dem Nokia N900 beschäftigen, zur Hilfe, "rootsh" auf dem Maemo System zu installieren, um root-Rechte auf dem Gerät anfordern zu können. Diese Anleitung ist eigentlich nur für absolute Neulinge gedacht, da diese Schritte von jedem/r erfahrenen Bastler-n/in/ix schnell selbstgemacht sind, ohne eine Anleitung zu benötigen.

## Voraussetzungen

Was wird alles dafür benötigt?

- ein technisch einwandfreies Nokia N900
- ein funktionierendes, internetangebundenes WLAN-Netzwerk
- ein eingerichteter Programm-Manager (Tutorial: [hier](../programmgr/tut_programmgr.md))

Was wird im Tutorial benutzt?

- ein technisch einwandfreies Nokia N900
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk

## Benötigte Dateien

**Keine**

### Besonderes

**Nichts**

## Kochrezept

1. Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900 und öffne den Progr.-Manager.

  Danach sollte der/die Anwender/in/ix auf `Herunterladen` drücken, danach auf `Alle`. Dort nach dem Paket `rootsh` suchen (einfach lostippen) und dieses installieren. Dabei wird der Benutzer/in/ix gefragt, welches WLAN-Netzwerk verwendet werden soll, wenn bisher keine Verbindung hergestellt worden ist.

  ![n900_packages](images/n900_packages.png)

Damit ist das Installieren von `rootsh` und somit auch das Freischalten der root-Rechte auf dem Nokia N900 abgeschlossen.
