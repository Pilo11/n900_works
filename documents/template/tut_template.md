# Beispiel Tutorial

## Präambel

Dieses Dokument ist eine Vorlage für folgende Tutorials und demonstriert Strukturen, die so auch weiterhin umgesetzt werden sollen!

## Voraussetzungen

Was wird alles dafür benötigt?

- ein Beispielgerät
- eine Beispielanwendung
- eine eingerichtete Beispielumgebung (Tutorial: [hier](../xxxxx/tut_xxxxxx.md))

Was wird im Tutorial benutzt?

- das Beispielgerät Nummer eins
- eine Standard Beispielanwendung

## Benötigte Dateien

**Keine**

___

ODER

___

Wie gewohnt, befinden sich für dieses Tutorial alle benötigten Dateien verschlüsselt in der MEGA Cloud.

**Wie kann ich die verschlüsselten Dateien verwenden/entschlüsseln?**

Dazu gibt es [hier](../fileHandling/tut_fileHandling.md) eine Anleitung.

- Dateien für dieses Tutorial: [hier](https://mega.nz/xxxxxxxxx)

- `example.txt`: Beispieldatei

### Besonderes

**Nichts**

___

ODER

___

- Besonderheit 1
- Besonderheit 2

## Automatisches Skript
**(Dieser Abschnitt ist nur dann einzufügen, wenn für das Tutorial ein automatisches Skript vorhanden ist.)**

Für dieses Kochrezept ist ein automatisches Skript vorhanden, welches benutzt werden kann. Diese Skripte sind immer ".sh" Dateien und können einfach mit `bash4 filename.sh` auf dem Gerät (im `X-Terminal`) ausgeführt werden.

**Das setzt voraus, dass `Bash4` bereits installiert wurde (wie es [hier](../bash4/tut_bash4.md) beschrieben ist).**

1. Man/Frau/Hermaphrodit/Neutrum starte das Nokia N900, schließe es an einen PC und ziehe die Datei `tut_xxxxx.sh` in das Hauptverzeichnis des Geräts.
1. Als nächstes müssen auch die Dateien (entschlüsselt) in einem Unterverzeichnis `files` auf das Gerät kopiert werden.
1. Danach wird X-Terminal geöffnet und folgendes eingegeben.

  `cd MyDocs`

  Zum Wechseln in das Hauptverzeichnis des internen Speichers.

  `bash4 tut_xxxxx.sh`

  Zum Ausführen des Skripts.

  Wenn dabei keine Fehler angezeigt werden, dann hat alles funktioniert.

## Kochrezept

1. Hier wird eine tolle Sache beschrieben.

  ![example](images/example.png)

Damit ist das Tutorial abgeschlossen.
