# Tutorial N900 Flash

## Präambel

TODO

## Voraussetzungen

Was wird alles dafür benötigt?

- ein sauberes oder gut gepflegtes `Maemo 5` System (falls nicht, gibt es [hier](../flash/tut_flash.md) ein Tutorial zum Flashen/Neuaufsetzen des Betriebssystems)
- ein funktionierendes Linux-OS
- ein technisch einwandfreies Nokia N900
- ein funktionierendes Micro-USB Kabel
- ein funktionierendes, internetangebundenes WLAN-Netzwerk
- root-Rechte (Tutorial: [hier](../rootsh/tut_rootsh.md))
- die bereitgestellten Dateien zum Thema `Arch Linux Installation`

Was wird im Tutorial benutzt?

- ein Laptop mit Arch Linux (Dell Latitude E5540)
- ein technisch einwandfreies Nokia N900 mit frisch installiertem `Maemo 5`
- ein "com-pad" Micro-USB Billig-Kabel
- ein Standard "Gammel" Kabel CBN Router von Vodafone Kabel für das WLAN-Netzwerk
- die bereitgestellten Dateien

## Benötigte Dateien

Wie gewohnt, befinden sich für dieses Tutorial alle benötigten Dateien verschlüsselt in der MEGA Cloud.

**Wie kann ich die verschlüsselten Dateien verwenden/entschlüsseln?**

Dazu gibt es [hier](../fileHandling/tut_fileHandling.md) eine Anleitung.

- Dateien für dieses Tutorial: [hier](https://mega.nz/#F!P00CRTyJ!DISMs6XH4UhBMwDJBPC7vQ)

## Kochrezept

1. Das Gerät (Nokia N900) muss mit dem PC verbunden werden. Nach kurzer Zeit fragt das Gerät, um was es sich für eine Verbindung handelt und man/frau/hermaphrodit/neutrum wählt dort `Massenspeichermodus` aus. Jetzt wird der Zugriff auf den internen Speicher des Geräts am PC bereitgestellt. Danach wird die Datei `liblzo2-2_2.03-1maemo3_armel.deb` aus `files` auf das Gerät kopiert (einfach in das Hauptverzeichnis).
1. Jetzt muss die Verbindung zum PC wieder getrennt werden (vorher unmount!!!). Danach wird die X-Terminal Applikation auf dem Gerät geöffnet und folgendes eingegeben.

  `sudo gainroot`

  ![n900_root](images/n900_root.png)

  Jetzt haben wir root-Rechte und können die `liblzo2-2_2` Bibliothek installieren. Aber vorher müssen wir auf das Verzeichnis des verfügbaren, internen Speichers wechseln.

  `cd MyDocs`

  Jetzt kann die Bibliothek installiert werden.

  ![n900_dpkg](images/n900_dpkg.png)

  `dpkg -i liblzo2-2_2.03-1maemo3_armel.deb`

  ![n900_dpkg_finished](images/n900_dpkg_finished.png)

  Nach der Installation kann das Paket mit `rm` gelöscht werden.

  Dann wird X-Terminal wieder geschlossen und der Programm-Manager geöffnet. Dort muss man/frau/hermaphrodit/neutrum das Paket `backupmenu` installieren. Dort können während der Installation mehrere Nachfragen kommen, diese bitte alle bejahen.

  ![n900_backupmenu](images/n900_backupmenu.png)
1. Jetzt muss das Gerät ausgeschaltet werden und die Verbindung zum PC sollte weiterhin getrennt bleiben. Nachdem es ausgeschaltet ist, sollte man/frau/hermaphrodit/neutrum noch etwa zehn Sekunden warten. Danach muss das Tastaturfeld im ausgeschalteten Zustand aufgefahren werden und das Gerät wieder starten. Danach erscheint das Boot Menu, dort muss mittels des Pfeiltastenkreuzes (auf der deutschen Tastatur existiert es nicht: oben ist "ä", links ist ".", rechts ist "->", unten ist "<-"). Dort muss der Menüpunkt `BackupMenu` ausgewählt werden. Jetzt muss das Gerät wieder mit dem PC verbunden werden. Danach muss `w` gedrückt werden.
1. Nun kann z.B. mit gParted eine Neupartitionierung vorgenommen werden.

  ![gparted_original](images/gparted_original.png)

  Im originalem, unberührtem Zustand sieht es aus wie oben. Ich tue folgendes (das kann auch anders gelöst werden). Ich lösche `sdb1` und erstelle davon eine neue Partition (ext4, 10GB, Label: arch). Danach erstelle ich von dem Rest eine neue Partition (fat32, restlicher Speicher, Label: Nokia N900). Wichtig ist dabei, dass die FAT32 Partition namens `Nokia N900` zuerst erstellt wird, damit diese die Partionsidentifikation `sdb1`  bekommt und vom Hauptsystem des Geräts erkannt wird.

  Theoretisch kann es auch reichen, die FAT32 Partition zu verkleinern und von davon die neue EXT4 Partition zu erstellen. Allerdings hatte das bei mir nicht geklappt, da ich auf Arch Linux arbeite und vorher vergessen hatte folgendes Paket zu installieren (ohne dieses Paket können mit gparted auch keine FAT32 Partitionen erstellt werden, es ist also dringend erforderlich! Paketname unter Arch Linux: `dosfstools`).

  ![gparted_finished](images/gparted_finished.png)

1. Jetzt muss das Gerät wieder gestartet werden (ohne dass die Tastatur ausgefahren ist). Als nächstes muss nämlich U-Boot installiert werden. Dazu wird die X-Terminal Applikation verwendet.

  ![n900_uboot_install](images/n900_uboot_install.png)

  Bei der Installation wird es wieder Abfragen, die akzeptiert werden müssen. Nach erfolgreicher Installation, muss das Gerät neugestartet werden (mit geöffnetem Tastaturfeld). Beim Neustart fällt sofort das U-Boot System auf, welches sich beim Booten des Gerätes dazwischenschaltet. Dies kann bisher noch ignoriert werden und falls gefragt wird, einfach bestätigen, ohne etwas Anderes auszuwählen.
1. Jetzt erscheint wieder das Boot-Menü. Jetzt wieder die Verbindung zum PC herstellen und die `w` Taste drücken. Als nächstes muss das Arch System auf die erstellte `arch` Partition kopiert werden. Bei mir funktionierte das nicht mit simplen Drag'n'Drop, da die Partition nicht für meinen Arch Linux benutzer beschreibbar ist. Daher habe ich das System per Konsole hinübergezogen und dort entpackt.

  Zuerst also in das Verzeichnis der `arch` Partition wechseln. Und mit folgendem Befehl das Arch System auf die Partition kopieren...

  `sudo cp /home/nutzer/N900-OS/arch-n900-20161204.tar.gz .`

  ... und dort auch entpacken.

  `sudo tar xvzf arch-n900-20161204.tar.gz`

  Danach ist das Tarball Archiv nicht mehr nützlich, daher kann es gelöscht werden.

  `sudo rm arch-n900-20161204.tar.gz`
1. Jetzt alle Partitionen sicher unmounten und das Gerät neustarten (ohne dass die Tastatur ausgefahren ist). Beim Start wieder den Massenspeichermodus auswählen und die `Nokia N900` Partition mounten. Dort dann aus dem Ordner `customScripts` die Datei `arch-boot.item` anpüassen, dann auf das Gerät kopieren und wieder unmounten. Auf dem Gerät muss diese Datei ins Verzeichnis `/etc/bootmenu.d/` verschoben werden (am besten mit X-Terminal).

  Es muss eigentlich nur eine Sache angepasst werden in der Datei und das ist die Partitionsidentifikation. Bisher steht in der Datei bei `ITEM_DEVICE` `${EXT_CARD}p1`. Das muss umgeschrieben werden zu `mmcblk0p4`, weil ich bei der Partitionierung in gParted für die `arch` Partition die Partitionsidentifikation sdb4 bekommen hatte. Demnach entsteht folgendes.

  `ITEM_DEVICE="mmcblk0p4"`

  UND

  `ITEM_CMDLINE="root=/dev/mmcblk0p4 rw rootwait init=/usr/lib/systemd/systemd omapfb.vram=0:3M,1:3M,2:3M quiet"`

  Die modifizierte, fertige Datei muss dann im Hauptverzeichnis des gemounteten, internen Speichers des Geräts übertragen werden.

  Danach wird auf dem Gerät wieder X-Terminal geöffnet.

  Dann müssen natürlich wieder root-Rechte angefordert werden (wie vor fast jedem Schritt mit X-Temrinal)
  `sudo gainroot`

  `cd MyDocs`

  Sollte der Ordner `bootmenu.d` in `/etc` nicht existieren, muss dieser vorher noch erstellt werden, sonst nicht. Eigentlich müsste dieser Ordner allerdings existieren, da bereits für `bootmenu` ein `item` angelegt sein müsste. Falls doch nicht, folgenden Befehl ausführen.

  `mkdir /etc/bootmenu.d`

  Danach die Datei verschieben.

  `mv arch-boot.menu /etc/bootmenu.d`

  Sollte im Ordner MyDocs seltsamerweise nichts sein (Ordner leer), dann darauf achten, dass die Verbindung zum PC getrennt ist und X-Terminal neustarten.

  Sonst anschließend nur noch das U-Boot Menü aktualisieren.

  `u-boot-update-bootmenu`

1. Dann wieder das Gerät neu starten (vom PC die Verbindung trennen, falls nicht) und bereit sein. Denn beim Start (während U-Boot erscheint), muss mit der Pfeiltaste nach unten der zweite Eintrag (Arch Linux) ausgewählt werden. Diese Taste also während des Bootens einfach mehrmals drücken, bis eine Abweichung vom üblichen Prozedere zu sehen ist.

TODO
